import { ref } from 'vue'
import { defineStore } from 'pinia'
interface AddressBook{
  id: number
  name: string
  tel: string
  gender: string
}
export const useAddressBookStore = defineStore('addrees_book', () => {
  let lastId = 1
   const address =ref<AddressBook>({
    id: 0 ,
    name: '',
    tel: '',
    gender: 'Male'
   })
   const addressList =ref<AddressBook[]>([])
   const isAddnew  = ref(false)
   function save(){
    if(address.value.id>0){
      const editIndex = addressList.value.findIndex((item) => item.id === address.value.id )
      addressList.value[editIndex] = address.value
    }else{
      addressList.value.push({...address.value,id: lastId++})
    }
    isAddnew.value = false
    
    address.value ={
      id: 0 ,
      name: '',
      tel: '',
      gender: 'Male'
    }
   }
   function edit(id: number){
    isAddnew.value =true
    const editIndex = addressList.value.findIndex((item) => item.id === id)
    address.value = JSON.parse(JSON.stringify(addressList.value[editIndex]))
   }
   function remove(id: number){
    const removeIndex = addressList.value.findIndex((item) => item.id === id)
    addressList.value.splice(removeIndex , 1 )
   }
   function cancle(){
    isAddnew.value = false 
    address.value ={
      id: 0 ,
      name: '',
      tel: '',
      gender: 'Male'
    }
    
   }

  return {address,addressList,save,isAddnew,edit,remove,cancle }
})
